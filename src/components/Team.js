import React from 'react';

class Team extends React.Component {
    state = {chutes : 0, pontos : 0}

    changeChutes() {
        this.setState({chutes : this.state.chutes + 1})

        const rand = Boolean(Math.round(Math.random()));
        if (rand === true){
        this.setState({pontos : this.state.pontos + 1})
        } else {
            this.setState({pontos : this.state.pontos})
        }
    }     
 
    render() {
        return(
            <div>
                <img src={this.props.logo} alt={"logo"} width={50}  />
                <br />
                Time = {this.props.name} 
                <br />
                Chutes = {this.state.chutes}
                <br />
                Pontos = {this.state.pontos}
                <br />
                <button onClick={() => this.changeChutes()}>Chutar</button>
            </div>
        )    
    }
}

export default Team; 