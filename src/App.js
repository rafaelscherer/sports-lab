import React from 'react';
import react from './React.png';
import javascript from './javascript.png';
import './App.css';
import Team from './components/Team';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h3>Welcome to the sports game starter</h3>
        <span className="Team">
        <Team logo={react} name="React" />
        <br />
        <Team logo={javascript} name="JavaScript" />
        </span>
       
      </header>
    </div>
  );
}

export default App;
